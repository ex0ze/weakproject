#!/bin/bash

WORK_DIR=$PWD
SQL_SCRIPT=$WORK_DIR/recreate_tables.sql
DB_HOST=sigsegv.space
DB_PORT=44444
DB_USER_ADMIN=postgres
DB_PASSWORD=$(cat postgres_pass)

function loadSqlScript
{
    sudo PGPASSWORD=$1 psql -h $2 -U $3 -p $4 -a -q -f $5
}

loadSqlScript $DB_PASSWORD $DB_HOST $DB_USER_ADMIN $DB_PORT $SQL_SCRIPT