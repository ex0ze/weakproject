#pragma once
#include <QString>
#include <QColor>

class AirportMapWidgetSettings
{
public:
    AirportMapWidgetSettings() = default;
    void loadSettings();

    QString mapTheme() const;
    int placeIconScaleFactor() const;
    QColor placeLineColor() const;
    int placeLineWidth() const;
    int titleXOffset() const;
    int titleYOffset() const;
    int titleFontSize() const;
    QColor titleFontColor() const;
private:
    QString mMapTheme;

    int mPlaceIconScaleFactor;
    QColor mPlaceLineColor;
    int mPlaceLineWidth;

    double mTitleXOffset;
    double mTitleYOffset;
    int mTitleFontSize;
    QColor mTitleFontColor;
};

