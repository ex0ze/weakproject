#pragma once

namespace queries
{

// login password
inline constexpr char kDatabaseLogin[] = "select * from ap_login('%1', '%2');";
// login pass fullname birthdate permission_level
inline constexpr char kDatabaseRegister[] = "select * from ap_register('%1', '%2', '%3', '%4', %5);";

// username
inline constexpr char kGetUserIdByUsername[] = "select id from users where username='%1'";

inline constexpr char kGetPlaneTypes[] = "select * from plane_types;";

// user_id flight_id plane_seat_id pieces_of_baggage
inline constexpr char kBookTicket[] = "select * from book_ticket(%1, %2, %3, %4)";

inline constexpr char kGetSeatTypes[] = "select * from seat_types;";

// name tail_number id_type
inline constexpr char kCreatePlane[] = "select * from create_plane('%1', '%2', %3);";
// plane_id
inline constexpr char kGetPlaneSeats[] = "select * from detailed_plane_seats where id = %1;";

// flight_id
inline constexpr char kGetPlaneSeats2[] = "select * from detailed_plane_seats_prices2 where flight_id = %1;";

// id_plane id_seat_type seats_count
inline constexpr char kAddPlaneSeats[] = "select * from add_plane_seats(%1, %2, %3);";

// name legal_address
inline constexpr char kCreateAirlines[] = "select * from create_airline('%1', '%2');";

inline constexpr char kGetAirlinesShort[] = "select id, name, legal_address from airlines;";
inline constexpr char kGetPlanesShort[] = "select id, name, tail_number from planes;";

inline constexpr char kGetAirports[] = "select * from airports;";

// departure_time departure_airport_id arrival_time arrival_airport_id flight_number airline_id plane_id
inline constexpr char kCreateFlight[] = "select * from create_flight('%1', %2, '%3', %4, '%5', %6, %7);";

// id
inline constexpr char kGetAirportsByFlightId[] = "select departure_airport_id, arrival_airport_id from flights where id = %1;";

// user_id
inline constexpr char kGetSummaryFlightsForUserId[] = "select * from flights_users_prices_summary where user_id = %1";
// id oldpass newpass
inline constexpr char kChangePassword[] = "select * from change_password(%1, '%2', '%3');";

// departure_time arrival_time
inline constexpr char kGetAllFlightsForPeriod[] = "select id, departure_time, arrival_time from flights where departure_time >= '%1' and arrival_time <= '%2'";

// departure_time arrival_time departure_id arrival_id
inline constexpr char kGetFilteredFlightsForPeriod[] = R"(
select id, departure_time, arrival_time from flights
where departure_time >= '%1' and arrival_time <= '%2'
and departure_airport_id = %3 and arrival_airport_id = %4;
)";

inline constexpr char kGetDepartureCountryWithBirthyear[] = "select * from get_departure_country_with_birthyear;";

// birth_year country
inline constexpr char kGetSeatsToCountryStats[] = "select * from flights_seats_birth_year_stats where birth_year = %1 and country = '%2';";

}
