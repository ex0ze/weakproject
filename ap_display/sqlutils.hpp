#pragma once
#include <QString>
#include <QSqlTableModel>
#include <set>
class QSqlDatabase;
class QTableView;
namespace util::sql {

inline constexpr char kSqlTimestampFormat[] = "yyyy-MM-dd hh:mm:ss";

void setReadonlyModel(QSqlDatabase db, QTableView * view, const QString& table, const std::set<int>& columnsToHide, const QString& filter = {});
void setReadonlyQueryBasedModel(QSqlDatabase db, QTableView * view, const QString& query, const std::set<int>& columnsToHide);
void setReadWriteTableModel(QSqlDatabase db, QTableView * view, const QString& table, const std::set<int>& columnsToHide = {}, const QString& filter = {});
void setReadWriteQueryModel(QSqlDatabase db, QTableView * view, const QString& query, const std::set<int>& columnsToHide = {});
void updateSqlTableBasedModel(QTableView * view);
void updateSqlQueryBasedModel(QTableView * view);

void setupAirportsTable(QTableView * view);

}

