#pragma once

#include <QWidget>
#include <memory>
#include <marble/MarbleWidget.h>
#include "airportprovider.hpp"
#include "airportmapwidgetsettings.hpp"
// класс мировой карты, предоставлящий интерфейс для добавления аэропортов для их последующей отрисовки
class AirportMapWidget : public Marble::MarbleWidget
{
public:
    using AirportsDrawMap = std::map<const Airport*, QString>;
    AirportMapWidget();
    bool connectAirports() const;
    void setConnectAirports(bool connectAirports);

public slots:
    void setAirports(AirportsDrawMap airports);

private:
    void initHideItems();
    void loadConfiguration();
    AirportsDrawMap mAirports;
    QPixmap mPixmap;
    bool mConnectAirports {false};
    std::unique_ptr<AirportMapWidgetSettings> pSettings;

    // MarbleWidget interface
protected:
    virtual void customPaint(Marble::GeoPainter *painter) override;
};

