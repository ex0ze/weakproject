#include "userwidget.hpp"
#include "ui_userwidget.h"
#include "sqlutils.hpp"
#include <set>
#include "logger.hpp"
#include <QMessageBox>
#include "airportdatabase.hpp"
#include "airportprovider.hpp"
#include "airportdatabasequeries.hpp"

UserWidget::UserWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserWidget)
{
    ui->setupUi(this);

    pMapWidget = new AirportMapWidget;
    ui->mapWidgetLayout->addWidget(pMapWidget);
    initFlightsBookTab();
    initFlightsUserTab();
    initUserTab();
}

UserWidget::~UserWidget()
{
    delete ui;
}

void UserWidget::initFlightsBookTab()
{
    util::sql::setReadonlyModel(AirportDatabase::instance().getConnection(), ui->bookFlightsView, "detailed_flights", {0}, QString(R"("Время вылета" > current_timestamp)"));

    auto loadSeatsInformation = [this](int flightId)
    {
        auto res = AirportDatabase::instance().getPlaneSeatsByFlightId(flightId).value_or(std::vector<AirportDatabase::PlaneSeatsPrice>{});
        ui->bookSeatTypeCombobox->clear();
        for (const auto& [flightId_, planeSeatId, typeName, bookedSeatsCount, allSeatsCount, price] : res)
        {
            ui->bookSeatTypeCombobox->addItem(QString("%1 (Занято мест %2/%3); Цена: %4").arg(typeName).arg(bookedSeatsCount).arg(allSeatsCount).arg(price),
                QVariant::fromValue(InternalSPComboboxData(planeSeatId, flightId, price, (allSeatsCount - bookedSeatsCount) > 0)));
        }
    };
    connect(this, &UserWidget::seatsInformationUpdated, loadSeatsInformation);
    connect(ui->bookFlightsView, &QTableView::clicked, [this, loadSeatsInformation](const QModelIndex& index) {
        int row = index.row();
        int flightId = ui->bookFlightsView->model()->index(row, 0).data().toInt();
        std::pair<int, int> airportIds = AirportDatabase::instance().getAirportsByFlightId(flightId).value_or(std::make_pair(-1, -1));
        if (airportIds.first == -1 && airportIds.second == -1)
            return;
        auto& currentAirports = mSelectedAirports[ui->bookTabWidget->currentIndex()];
        currentAirports.first = AirportProvider::instance().getAirport(airportIds.first);
        currentAirports.second = AirportProvider::instance().getAirport(airportIds.second);
        drawAirportsOnMap();
        loadSeatsInformation(flightId);
        auto oldIndex = ui->bookFlightsView->currentIndex();
        util::sql::updateSqlTableBasedModel(ui->bookFlightsView);
        ui->bookFlightsView->setCurrentIndex(oldIndex);
    });
    connect(ui->bookTabWidget, &QTabWidget::currentChanged, this, &UserWidget::drawAirportsOnMap);
    connect(ui->bookBtn, &QPushButton::clicked, [this] {
        InternalSPComboboxData cbData;
        if (auto data = ui->bookSeatTypeCombobox->currentData(); data.isValid())
            cbData = data.value<InternalSPComboboxData>();
        else
        {
            QMessageBox::critical(this, "Ошибка бронирования", "Выберите пожалуйста тип места");
            return;
        }
        auto [planeSeatId, flightId, price, canBook] = cbData;
        if (!canBook)
        {
            QMessageBox::critical(this, "Ошибка бронирования", "Места по выбранному вами билету кончились");
            return;
        }
        int piecesOfBaggage = ui->bookBaggageSpinbox->value();
        if (QMessageBox::StandardButton::Yes == QMessageBox::question(this, "Бронирование", QString("Вы точно хотите забронировать билет за %1?").arg(price)))
        {
            auto maybeRes = AirportDatabase::instance().bookTicket(AirportDatabase::instance().getCurrentUser().id(), flightId, planeSeatId, piecesOfBaggage);
            if (!maybeRes.has_value())
            {
                QMessageBox::critical(this, "Ошибка бронирования", "Произошла системная ошибка");
                return;
            }
            if (maybeRes.value())
            {
                QMessageBox::information(this, "Бронирование", "Билет был успешно забронирован");
            }
            else
            {
                QMessageBox::critical(this, "Ошибка бронирования", "Места по выбранному вами билету кончились");
            }
            emit seatsInformationUpdated(flightId);
            emit flightsInformationUpdated();
        }
        else
            return;

    });
}

void UserWidget::initFlightsUserTab()
{
    util::sql::setReadonlyQueryBasedModel(AirportDatabase::instance().getConnection(),
        ui->userFlightsView, QString(queries::kGetSummaryFlightsForUserId).arg(AirportDatabase::instance().getCurrentUser().id()), {0, 1, 2, 4});
    connect(this, &UserWidget::flightsInformationUpdated, [this] {
        util::sql::updateSqlQueryBasedModel(ui->userFlightsView);
    });
    connect(ui->userFlightsView, &QTableView::clicked, [this](const QModelIndex& index) {
        constexpr int srcApIdx = 2;
        constexpr int dstApIdx = 4;
        int row = index.row();
        int srcApId = ui->userFlightsView->model()->index(row, srcApIdx).data().toInt();
        int dstApId = ui->userFlightsView->model()->index(row, dstApIdx).data().toInt();
        auto& selectedAirports = getSelectedAirports();
        selectedAirports.first = AirportProvider::instance().getAirport(srcApId);
        selectedAirports.second = AirportProvider::instance().getAirport(dstApId);
        drawAirportsOnMap();
    });
}

void UserWidget::initUserTab()
{
    const auto& user = AirportDatabase::instance().getCurrentUser();
    ui->userBirthDateLabel->setText(user.birthDate().toString());
    ui->userFullNameLabel->setText(user.fullName());
    ui->userLoginLabel->setText(user.userName());
    connect(ui->userChangePasswordBtn, &QPushButton::clicked, [this] {
        QString oldPass = ui->userOldPassword->text();
        QString newPass1 = ui->userNewPassword1->text();
        QString newPass2 = ui->userNewPassword2->text();
        if (oldPass.isEmpty() || newPass1.isEmpty() || newPass2.isEmpty())
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Заполните пожалуйста все поля");
            return;
        }
        if (newPass1 != newPass2)
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Новые пароли не совпадают");
            return;
        }
        auto maybeRes = AirportDatabase::instance().changePassword(AirportDatabase::instance().getCurrentUser().id(), oldPass, newPass1);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка", "Произошла системная ошибка, повторите позже");
            return;
        }
        if (maybeRes.value())
        {
            QMessageBox::information(this, "Успешно", "Пароль обновлён");
        }
        else
        {
            QMessageBox::critical(this, "Ошибка смены пароля", "Вы ввели старый пароль неверно");
        }
    });
}

std::pair<const Airport *, const Airport *> &UserWidget::getSelectedAirports()
{
    return mSelectedAirports[ui->bookTabWidget->currentIndex()];
}

void UserWidget::drawAirportsOnMap()
{
    auto& currentAirports = mSelectedAirports[ui->bookTabWidget->currentIndex()];
    pMapWidget->setConnectAirports(true);
    pMapWidget->setAirports({ {currentAirports.first, "Точка отправки"}, {currentAirports.second, "Точка прибытия"} });
    pMapWidget->update();
}
