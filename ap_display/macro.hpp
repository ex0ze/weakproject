#pragma once

#define DISABLE_COPY_MOVE(cl) \
cl(const cl&) = delete; \
cl& operator=(const cl&) = delete; \
cl(cl&&) = delete; \
cl& operator=(cl&&) = delete;
