#pragma once

#include <QWidget>
#include <vector>
#include "flightscountchartview.hpp"
#include "seatscountriescharview.hpp"


class Airport;

namespace Ui {
class InfographicWidget;
}
// управляющий класс виджета инфографиков, объединяющий в себе все инфографики
class InfographicWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InfographicWidget(QWidget *parent = nullptr);
    ~InfographicWidget();

private:
    void initFlightsTab();
    void initSeatsTab();
    void updateSeatsTab();
    Ui::InfographicWidget *ui;
    FlightsCountChartView * pFlightsCountChart;
    SeatsCountriesCharView * pSeatsCountriesChart;
    std::pair<const Airport*, const Airport*> mFlightsSelectedAirports;
    std::map<int, std::set<QString>> mSeatsComboboxData;
};

