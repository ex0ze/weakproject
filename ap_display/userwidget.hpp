#pragma once

#include <QWidget>
#include "airportmapwidget.hpp"

namespace Ui {
class UserWidget;
}
// Класс, отвечающий за информационную панель пользователя и соединяющий все виджеты на ней
class UserWidget : public QWidget
{
    Q_OBJECT
public:
    // planeSeatId, flightId, price, allowed to book a ticket
    using InternalSPComboboxData = std::tuple<int, int, QString, bool>;
    explicit UserWidget(QWidget *parent = nullptr);
    ~UserWidget();
signals:
    void seatsInformationUpdated(int flightId);
    void flightsInformationUpdated();
private:
    Ui::UserWidget *ui;
    AirportMapWidget * pMapWidget;
    void initFlightsBookTab();
    void initFlightsUserTab();
    void initUserTab();

    std::map<int, std::pair<const Airport*, const Airport*>> mSelectedAirports {
        {0, {nullptr, nullptr}},
        {1, {nullptr, nullptr}}
    };
    std::pair<const Airport*, const Airport*>& getSelectedAirports();
private slots:
    void drawAirportsOnMap();
};

Q_DECLARE_METATYPE(UserWidget::InternalSPComboboxData)
