#pragma once
#include <QSettings>
#include <iostream>
#include <QFile>
#include <QDir>
#include <QColor>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <fstream>
#include <filesystem>

namespace util
{
constexpr char kDefaultConfigDir[]  = "config";
constexpr char kDefaultConfigName[] = "config.ini";
constexpr char kDefaultConfigPath[] = "config/config.ini";

inline bool init_config()
{
    std::filesystem::path abs_dir = std::filesystem::absolute(std::filesystem::current_path()) / kDefaultConfigDir;
    auto abs_path = abs_dir / kDefaultConfigName;
    if (!std::filesystem::exists(abs_path))
    {
        if (!std::filesystem::exists(abs_dir))
        {
            std::error_code ec;
            bool ok = std::filesystem::create_directories(abs_dir, ec);
            if (ec || !ok)
            {
                std::cerr << __FILE__ << " " << __LINE__ << " error creating config: " << ec.message() << std::endl;
                return false;
            }
        }
        QFile res_config(":/config.ini");
        res_config.open(QIODevice::ReadOnly);
        auto bytes = res_config.readAll();
        std::ofstream out(abs_path, std::ios::binary);
        out.write(bytes.constData(), bytes.size());
        out.close();
        return true;
    }
    return true;
}

inline std::optional<QColor> parse_color(const QString& color)
{
    static QRegularExpression regex(R"(\(\s*?(?P<Red>\d+)\s*?\,\s*?(?P<Green>\d+)\s*?\,\s*?(?P<Blue>\d+)\s*?\,\s*?(?P<Alpha>\d+)\s*?\))");
    auto match = regex.match(color);
    if (!match.hasMatch())
        return std::nullopt;
    int r = match.captured("Red").toInt();
    int g = match.captured("Green").toInt();
    int b = match.captured("Blue").toInt();
    int a = match.captured("Alpha").toInt();
    return QColor(r, g, b, a);
}

inline QSettings& config()
{
    static QSettings settings(kDefaultConfigPath, QSettings::Format::IniFormat);
    return settings;
}

}
