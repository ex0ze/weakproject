#include "seatscountriescharview.hpp"
#include <QPieSeries>

SeatsCountriesCharView::SeatsCountriesCharView(QWidget *parent) : QtCharts::QChartView(parent)
{
    setRenderHint(QPainter::Antialiasing);
}

void SeatsCountriesCharView::setSeatsToCountryStats(std::vector<AirportDatabase::FlightsSeatsBirthYear> stats)
{
    mStats.clear();
    for (auto& [seatType, count] : stats)
    {
        mStats[std::move(seatType)] = count;
    }
    processDataset();
}

void SeatsCountriesCharView::processDataset()
{
    int sum = std::accumulate(mStats.begin(), mStats.end(), 0, [&](int init, auto p) { return init + p.second; });

    auto chart = this->chart();
    chart->removeAllSeries();
    auto axis = chart->axes();
    for (auto a : axis)
        chart->removeAxis(a);
    auto series = new QtCharts::QPieSeries;
    for (auto& [seatType, count] : mStats)
    {
        series->append(QString("%1, кол-во мест: %2 (%3 %)")
                       .arg(seatType)
                       .arg(count)
                       .arg(QString::number(static_cast<double>(count) / sum * 100.0, 'g', 3)), count);
    }
    auto slices = series->slices();
    chart->addSeries(series);
}
