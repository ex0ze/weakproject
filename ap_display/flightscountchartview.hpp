#pragma once
#include <QChartView>
#include <QChart>
#include <QDate>
#include <set>
#include "airportdatabase.hpp"
// класс модели инфорграфика, отображающий количество полётов за указанную дату
class FlightsCountChartView : public QtCharts::QChartView
{
public:
    FlightsCountChartView(QWidget * parent = nullptr);
    void setFlights(std::vector<AirportDatabase::FlightDt> flights);
private:
    void processDataset();
    static std::set<QDate> getDatesInPeriod(const QDateTime& beg, const QDateTime& end);
    std::vector<AirportDatabase::FlightDt> mFlights;
};

