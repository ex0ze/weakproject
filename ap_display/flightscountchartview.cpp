#include "flightscountchartview.hpp"
#include <QBarSet>
#include <QBarSeries>
#include <QRubberBand>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <map>

FlightsCountChartView::FlightsCountChartView(QWidget *parent) : QtCharts::QChartView(parent)
{
    setRenderHint(QPainter::Antialiasing);
    setRubberBand(QtCharts::QChartView::RubberBand::HorizontalRubberBand);
}

void FlightsCountChartView::setFlights(std::vector<AirportDatabase::FlightDt> flights)
{
    mFlights = std::move(flights);
    processDataset();
}

void FlightsCountChartView::processDataset()
{
    auto chart = this->chart();
    chart->removeAllSeries();
    auto axes_ = chart->axes();
    for (auto a : axes_)
        chart->removeAxis(a);
    std::map<QDate, std::size_t> counter;
    for (const auto& [id, dtBegin, dtEnd] : mFlights)
    {
        for (const auto& d : getDatesInPeriod(dtBegin, dtEnd))
            ++counter[d];
    }
    auto set = new QtCharts::QBarSet("Количество полётов за дату");
    auto * series = new QtCharts::QBarSeries;
    QStringList categories;
    std::size_t maxCount = 0;
    for (const auto& [date, count] : counter)
    {
        maxCount = std::max(maxCount, count);
        set->append(count);
        categories.append(date.toString("dd.MM.yyyy"));
    }
    series->append(set);
    chart->addSeries(series);
    chart->setTitle("Статистика полётов");
    chart->setAnimationOptions(QtCharts::QChart::SeriesAnimations);

    auto axisX = new QtCharts::QBarCategoryAxis;
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    auto axisY = new QtCharts::QValueAxis;
    axisY->setRange(0, maxCount);
    axisY->setTickType(QtCharts::QValueAxis::TickType::TicksFixed);
    axisY->setTickCount(maxCount + 1);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
}

std::set<QDate> FlightsCountChartView::getDatesInPeriod(const QDateTime &beg, const QDateTime &end)
{
    QDate curr = beg.date();
    QDate endDate = end.date().addDays(1);
    std::set<QDate> res;
    while (curr != endDate)
    {
        res.emplace(curr);
        curr = curr.addDays(1);
    }
    return res;
}
