#include "mainwindow.hpp"
#include "./ui_mainwindow.h"
#include "logger.hpp"
#include "airportdatabase.hpp"
#include "userwidget.hpp"
#include "adminwidget.hpp"
#include <QMessageBox>
#include <QCloseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadModel()
{
    const auto & user = AirportDatabase::instance().getCurrentUser();
    if (!user.isAuthorized())
    {
        util::log()->critical("Attempt to load model while unauthorized");
        return;
    }
    if (user.privileges() == AirportUser::Privileges::User)
    {
        ui->gridLayout->addWidget(new UserWidget);
    }
    else if (user.privileges() == AirportUser::Privileges::Admin)
    {
        ui->gridLayout->addWidget(new AdminWidget);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    auto res = QMessageBox::question(this, "Выход", "Вы действительно хотите выйти?");
    if (QMessageBox::StandardButton::Yes == res)
    {
        event->accept();
        emit exitRequested();
    }
    else
    {
        event->ignore();
    }
}

