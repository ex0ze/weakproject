#pragma once
#include "config.hpp"
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

namespace util
{

inline constexpr char logger_name[] = "logger";

inline void init_log(const std::string& filename = "log.txt")
{
    auto path = config().value("Logging/log_dir", "logs").toString().toStdString() + "/" + filename;
    auto flush_timeout_s = config().value("Logging/flush_timeout_s", 3).toInt();
    spdlog::basic_logger_mt(logger_name, path);
    spdlog::get(logger_name)->set_level(spdlog::level::debug);
    spdlog::flush_every(std::chrono::seconds(flush_timeout_s));
}

inline std::shared_ptr<spdlog::logger> log()
{
    return spdlog::get(logger_name);
}

};
