#pragma once

#include <QChartView>
#include "airportdatabase.hpp"
// класс виджета инфографика, отображающий процентное соотношение использования различных мест в самолёте
// в зависимости от выбранной страны и года рождения пассажиров
class SeatsCountriesCharView : public QtCharts::QChartView
{
public:
    SeatsCountriesCharView(QWidget * parent = nullptr);
    void setSeatsToCountryStats(std::vector<AirportDatabase::FlightsSeatsBirthYear> stats);
private:
    void processDataset();
    std::map<QString, int> mStats;
};



