#pragma once
#include <QCryptographicHash>
#include <QByteArray>

namespace util
{

inline QByteArray keccak256hash(const QByteArray& data)
{
    QCryptographicHash hasher(QCryptographicHash::Algorithm::Keccak_256);
    hasher.addData(data);
    return hasher.result().toHex();
}

}
