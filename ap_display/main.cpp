#include "mainwindow.hpp"
#include <QApplication>
#include <QSettings>
#include <QCryptographicHash>
#include "logger.hpp"
#include "loginregisterdialog.hpp"
#include "airportdatabase.hpp"
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QObject::connect(&w, &MainWindow::exitRequested, [] {
        util::log()->info("Закрытие приложения по запросу пользователя");
        exit(0);
    });
    if (!util::init_config())
        return 1;
    util::init_log();
    util::log()->info("Initializing done");
    if (!AirportDatabase::instance().init())
    {
        QMessageBox::critical(nullptr, "Ошибка инициализации", "Возможно, введены неверные логин-пароль для БД, проверьте логи");
        return 1;
    }
    util::log()->info("Program successfully started");
    LoginRegisterDialog dlg;
    QObject::connect(&dlg, &LoginRegisterDialog::successfullyAuthorized, [&] {
        w.loadModel();
        w.show();
    });
    dlg.show();
    return a.exec();
}
