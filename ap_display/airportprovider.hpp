#pragma once
#include <QString>
#include <unordered_map>
#include <map>
#include "macro.hpp"

class QSqlDatabase;

struct Airport
{
    int mId;
    QString mName;
    QString mIcaoCode;
    QString mCountry;
    double mLatitude;
    double mLongitude;
};

// глобальный класс, хранящий информацию о всех аэропортах в системе

class AirportProvider
{
    DISABLE_COPY_MOVE(AirportProvider);
public:
    // хранение аэропортов по порядку для заполнения таблиц
    using AirportsMap = std::map<int, Airport>;
    // быстрое получение аэропорта по id для поиска
    using AirportsHashView = std::unordered_map<int, Airport*>;
    // юзается для табличных моделей аэропортов
    using AirportsVecView = std::vector<Airport*>;

    static AirportProvider& instance();

    const AirportsMap& getAirports() const noexcept;
    AirportsMap& getAirports() noexcept;
    Airport * getAirport(int id) const;
    AirportsVecView& getView() noexcept;
    const AirportsVecView& getView() const noexcept;
    void createView();
private:
    AirportProvider() = default;
    AirportsMap mAirports;
    AirportsHashView mHashView;
    AirportsVecView mVecView;
};

