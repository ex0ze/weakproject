#include "airportprovider.hpp"

AirportProvider &AirportProvider::instance()
{
    static AirportProvider provider;
    return provider;
}

const AirportProvider::AirportsMap &AirportProvider::getAirports() const noexcept
{
    return mAirports;
}

AirportProvider::AirportsMap &AirportProvider::getAirports() noexcept
{
    return mAirports;
}

Airport * AirportProvider::getAirport(int id) const
{
    if (auto it = mHashView.find(id); it != mHashView.end())
    {
        return it->second;
    }
    else
    {
        return nullptr;
    }
}

const AirportProvider::AirportsVecView &AirportProvider::getView() const noexcept
{
    return mVecView;
}

AirportProvider::AirportsVecView &AirportProvider::getView() noexcept
{
    return mVecView;
}

void AirportProvider::createView()
{
    mHashView.reserve(mAirports.size());
    for (auto& [key, value] : mAirports)
    {
        mHashView.emplace(key, &value);
        mVecView.emplace_back(&value);
    }
}
