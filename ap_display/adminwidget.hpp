#pragma once

#include <QWidget>
#include "airportmapwidget.hpp"
#include "airportdatabase.hpp"

class InfographicWidget;

namespace Ui {
class AdminWidget;
}

// Класс, отвечающий за информационную панель администратора и соединяющий все виджеты на ней

class AdminWidget : public QWidget
{
    Q_OBJECT
    enum class SelectedAirportType
    {
        Departure,
        Arrival,
        Both
    };

public:
    explicit AdminWidget(QWidget *parent = nullptr);
    ~AdminWidget();
public slots:
    void updatePlaneModel();
    void updateAirlinesView();
signals:
    void airlinesUpdated();
    void flightsUpdated();
    void planesUpdated();
    void airportSelected(Airport * apPtr, SelectedAirportType apType);
private:
    void initConnections();
    void initTables();

    void initAirlinesTab();
    void initPlanesTab();
    void initFlightsTab();
    void initFlightsEditTab();
    void fillFlightsAirportsTable();

    Ui::AdminWidget *ui;
    AirportDatabase::PlaneTypes mPlaneTypes;
    AirportDatabase::SeatTypes mSeatTypes;
    int mCurrentPlaneId = -1;
    std::map<int, std::pair<const Airport*, const Airport*>> mSelectedAirports {
        {0, {nullptr, nullptr}},
        {1, {nullptr, nullptr}}
    };
    AirportMapWidget * pMapWidget;
    InfographicWidget * pInfographicWidget;
private slots:
    void drawAirportOnMap(Airport * apPtr, SelectedAirportType apType);
};

