#include "airportmapwidget.hpp"
#include "logger.hpp"
#include "config.hpp"
#include <marble/GeoDataDocument.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoDataTreeModel.h>
#include <marble/GeoPainter.h>
#include <marble/MarbleModel.h>
#include <marble/AbstractFloatItem.h>
#include <set>
#include <QFile>

AirportMapWidget::AirportMapWidget()
{
    loadConfiguration();
    initHideItems();
    centerOn(61.0, 38.0);
    setDistance(5000.0);
}

void AirportMapWidget::setAirports(AirportsDrawMap airports)
{
    mAirports = std::move(airports);
}

void AirportMapWidget::initHideItems()
{
    const std::set<QString> itemsToHide { "navigation", "compass", "scalebar", "license" };
    auto fItems { floatItems() };
    for (auto * item : fItems)
    {
        if (item && itemsToHide.count(item->nameId()))
            item->hide();
    }
    setShowGrid(false);
    setShowCrosshairs(false);
}

void AirportMapWidget::loadConfiguration()
{
    pSettings.reset(new AirportMapWidgetSettings);
    pSettings->loadSettings();
    setMapThemeId(pSettings->mapTheme());
    setProjection(Marble::Projection::Mercator);
    QFile f(":/place.png");
    if (!f.exists())
    {
        util::log()->critical("Can't load {} from resources at {} {}", f.fileName().toStdString(), __FILE__, __func__);
    }
    else
    {
        f.open(QIODevice::ReadOnly);
        auto data = f.readAll();
        mPixmap.loadFromData(data, "PNG");
        mPixmap = mPixmap.scaled(mPixmap.width() / pSettings->placeIconScaleFactor(), mPixmap.height() / pSettings->placeIconScaleFactor());
    }
}

bool AirportMapWidget::connectAirports() const
{
    return mConnectAirports;
}

void AirportMapWidget::setConnectAirports(bool connectAirports)
{
    mConnectAirports = connectAirports;
}

void AirportMapWidget::customPaint(Marble::GeoPainter *painter)
{
    const Airport * prev = nullptr;
    const Airport * current = nullptr;
    for (const auto& [airportPtr, title] : mAirports)
    {
        if (!airportPtr)
            continue;
        current = airportPtr;
        Marble::GeoDataCoordinates coords(airportPtr->mLongitude, airportPtr->mLatitude, 0.0, Marble::GeoDataCoordinates::Degree);
        auto font = painter->font();
        font.setBold(true);
        font.setPointSize(pSettings->titleFontSize());
        painter->setFont(font);
        painter->setPen(pSettings->titleFontColor());
        painter->drawText(coords, title, pSettings->titleXOffset(), pSettings->titleYOffset());
        painter->drawPixmap(coords, mPixmap);
        if (mConnectAirports)
        {
            if (prev)
            {
                Marble::GeoDataLineString ls;
                ls.append(Marble::GeoDataCoordinates(prev->mLongitude, prev->mLatitude, 0.0, Marble::GeoDataCoordinates::Degree));
                ls.append(coords);
                painter->setPen(QPen(QBrush(pSettings->placeLineColor()), pSettings->placeLineWidth()));
                painter->drawPolyline(ls);
            }
        }
        prev = airportPtr;
    }
}

