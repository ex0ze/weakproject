#include "airportdatabase.hpp"
#include "airportdatabasequeries.hpp"
#include "keccak256hasher.hpp"
#include "airportprovider.hpp"
#include "sqlutils.hpp"
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
AirportDatabase &AirportDatabase::instance()
{
    static AirportDatabase instance;
    return instance;
}

AirportUser &AirportDatabase::getCurrentUser() noexcept
{
    return mCurrentUser;
}

const AirportUser &AirportDatabase::getCurrentUser() const noexcept
{
    return mCurrentUser;
}

std::optional<bool> AirportDatabase::login(const QString &username, const QString &password)
{
    QString hashedPassword = util::keccak256hash(password.toUtf8());
    mCurrentUser.setIsAuthorized(false);
    QString queryStr = QString(queries::kDatabaseLogin).arg(username).arg(hashedPassword);
    QSqlQuery query(getUserConnection());
    if (!executeQuery(query, queryStr, 1, 4))
        return false;
    query.next();
    if (query.isNull(0))
    {
        util::log()->info("Failed to login with username '{}' and password '{}'", username.toStdString(), hashedPassword.toStdString());
        return false;
    }
    else
        util::log()->info("Successfully logged with username '{}' and password '{}'", username.toStdString(), hashedPassword.toStdString());
    auto maybePermLevel = AirportUser::privilegesForNumber(query.value(0).toInt());
    if (!maybePermLevel.has_value())
    {
        util::log()->critical("Got incorrect privileges level from database");
        return std::nullopt;
    }
    QString fullName = query.value(2).toString();
    QDate dateOfBirth = query.value(3).toDate();
    mCurrentUser.setPrivileges(maybePermLevel.value());
    mCurrentUser.setFullName(fullName);
    mCurrentUser.setBirthDate(dateOfBirth);
    mCurrentUser.setUserName(username);
    mCurrentUser.setIsAuthorized(true);
    obtainUserId();
    return true;
}

std::optional<bool> AirportDatabase::reg(AirportUser newUser)
{
    newUser.setPassword(util::keccak256hash(newUser.password().toUtf8()));
    mCurrentUser.setIsAuthorized(false);
    auto maybePriv = AirportUser::intForPrivileges(newUser.privileges());
    if (!maybePriv.has_value())
    {
        util::log()->critical("Got incorrect privileges level from user");
        return false;
    }
    int priv = maybePriv.value();
    QString queryStr = QString(queries::kDatabaseRegister)
                           .arg(newUser.userName())
                           .arg(newUser.password())
                           .arg(newUser.fullName())
                           .arg(newUser.birthDate().toString(Qt::DateFormat::ISODate))
                           .arg(priv);
    QSqlQuery query(getUserConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return false;
    query.next();
    bool result = query.value(0).toBool();
    if (result)
    {
        newUser.setPassword({});
        mCurrentUser = std::move(newUser);
        mCurrentUser.setIsAuthorized(true);
    }
    obtainUserId();
    return result;
}

bool AirportDatabase::init()
{
    util::log()->info("Checking database credentials...");
    auto& cfg = util::config();
    auto userConnection = QSqlDatabase::addDatabase("QPSQL", "user_db");
    auto adminConnection = QSqlDatabase::addDatabase("QPSQL", "admin_db");
    QString database_host = cfg.value("Database/ap_database_host").toString();
    QString database_name = cfg.value("Database/ap_database_name").toString();
    if (database_host.isEmpty() || database_name.isEmpty())
    {
        util::log()->critical("Database host or name are empty");
        return false;
    }
    int database_port = cfg.value("Database/ap_database_port", 5432).toInt();
    std::pair<QString, QString> user_creds {
        cfg.value("Database/ap_user_login").toString(),
        cfg.value("Database/ap_user_password").toString()
    };
    std::pair<QString, QString> admin_creds {
        util::config().value("Database/ap_admin_login").toString(),
        util::config().value("Database/ap_admin_password").toString()
    };
    if (user_creds.first.isEmpty() || admin_creds.first.isEmpty())
    {
        util::log()->critical("User or admin credentials are empty");
        return false;
    }
    userConnection.setDatabaseName(database_name);
    adminConnection.setDatabaseName(database_name);

    userConnection.setHostName(database_host);
    adminConnection.setHostName(database_host);

    userConnection.setPort(database_port);
    adminConnection.setPort(database_port);

    userConnection.setUserName(user_creds.first);
    userConnection.setPassword(user_creds.second);

    adminConnection.setUserName(admin_creds.first);
    adminConnection.setPassword(admin_creds.second);
    bool user_ok = adminConnection.open();
    if (!user_ok)
    {
        auto err = adminConnection.lastError();
        util::log()->critical("Error connecting to user database: {}", err.text().toStdString());
        return false;
    }
    bool admin_ok = adminConnection.open();
    if (!admin_ok)
    {
        auto err = adminConnection.lastError();
        util::log()->critical("Error connecting to admin database: {}", err.text().toStdString());
        return false;
    }
    util::log()->info("Credentials are valid");

    util::log()->info("Filling airports provider...");

    QString queryStr = QString(queries::kGetAirports);
    QSqlQuery query(getUserConnection());
    if (!executeQuery(query, queryStr, -1, 6))
    {
        util::log()->critical("Error while filling airports provider occured");
        return false;
    }
    auto& airportsMap = AirportProvider::instance().getAirports();
    while (query.next())
    {
        Airport ap;
        ap.mId = query.value(0).toInt();
        ap.mName = query.value(1).toString();
        ap.mIcaoCode = query.value(2).toString();
        ap.mCountry = query.value(3).toString();
        ap.mLatitude = query.value(4).toDouble();
        ap.mLongitude = query.value(5).toDouble();
        airportsMap.emplace(ap.mId, std::move(ap));
    }
    AirportProvider::instance().createView();
    return true;
}

bool AirportDatabase::executeQuery(QSqlQuery &query, const QString& queryStr, int numRowsExpected, int numColsExpected) const
{
    bool ok = query.exec(queryStr);
    util::log()->debug("Executing SQL request '{}' : {}", queryStr.toStdString(), ok ? "success" : "failed");
    if (!ok)
        util::log()->debug("Driver error: {} | Database Error: {}", query.lastError().driverText().toStdString(), query.lastError().databaseText().toStdString());
    if (!ok)
        return false;
    int rowCount = query.size();
    if (numRowsExpected >= 0 && rowCount != numRowsExpected)
    {
        util::log()->critical("Returned wrong number of rows: got {}, expected {}", rowCount, numRowsExpected);
        return false;
    }
    if (rowCount > 0)
    {
        query.next();
        if (numColsExpected >= 0 && query.record().count() != numColsExpected)
        {
            util::log()->critical("Returned wrong number of cols: got {}, expected {}", query.record().count(), numColsExpected);
            return false;
        }
        query.previous();
        return true;
    }
    return true;
}

void AirportDatabase::obtainUserId()
{
    QString queryStr = QString(queries::kGetUserIdByUsername).arg(mCurrentUser.userName());
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
    {
        util::log()->info("Ошибка получения id по имени пользователя {}", mCurrentUser.userName().toStdString());
        std::exit(-1);
    }
    query.next();
    mCurrentUser.setId(query.value(0).toInt());
}

QSqlDatabase AirportDatabase::getAdminConnection() const
{
    return QSqlDatabase::database("admin_db");
}

QSqlDatabase AirportDatabase::getConnection() const
{
    if (mCurrentUser.isAuthorized())
    {
        auto priv = mCurrentUser.privileges();
        if (priv == AirportUser::Privileges::User)
            return getUserConnection();
        else if (priv == AirportUser::Privileges::Admin)
            return getAdminConnection();
        else
        {
            util::log()->critical("Attempt to get connection for unknown priv level {}, user {}", static_cast<int>(priv), mCurrentUser.userName().toStdString());
            std::exit(-1);
        }
    }
    else
    {
        util::log()->critical("Attempt to get connection for unauthorized user");
        std::exit(-1);
    }
}

std::optional<AirportDatabase::PlaneTypes> AirportDatabase::getPlaneTypes() const
{
    QString queryStr = QString(queries::kGetPlaneTypes);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 2))
        return std::nullopt;
    AirportDatabase::PlaneTypes res;
    while (query.next())
    {
        res.emplace(query.value(0).toInt(), query.value(1).toString());
    }
    return res;
}

std::optional<AirportDatabase::SeatTypes> AirportDatabase::getSeatTypes() const
{
    QString queryStr = QString(queries::kGetSeatTypes);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 2))
        return std::nullopt;
    AirportDatabase::SeatTypes res;
    while (query.next())
    {
        res.emplace(query.value(0).toInt(), query.value(1).toString());
    }
    return res;
}

std::optional<bool> AirportDatabase::createPlane(const QString &name, const QString &tailNumber, int idType) const
{
    QString queryStr = QString(queries::kCreatePlane).arg(name).arg(tailNumber).arg(idType);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return std::nullopt;
    query.next();
    return query.value(0).toBool();
}

std::optional<bool> AirportDatabase::addPlaneSeats(int planeId, int seatTypeId, int seatsCount) const
{
    QString queryStr = QString(queries::kAddPlaneSeats).arg(planeId).arg(seatTypeId).arg(seatsCount);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return std::nullopt;
    query.next();
    return query.value(0).toBool();
}

std::optional<bool> AirportDatabase::bookTicket(int userId, int flightId, int planeSeatId, int piecesOfBaggage) const
{
    QString queryStr = QString(queries::kBookTicket).arg(userId).arg(flightId).arg(planeSeatId).arg(piecesOfBaggage);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return std::nullopt;
    query.next();
    return query.value(0).toBool();
}

std::optional<bool> AirportDatabase::createAirlines(const QString &name, const QString &legalAddress)
{
    QString queryStr = QString(queries::kCreateAirlines).arg(name).arg(legalAddress);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return std::nullopt;
    query.next();
    return query.value(0).toBool();
}

std::optional<bool> AirportDatabase::createFlight(const QDateTime &departureDt, const QDateTime &arrivalDt, int departureAirportId, int arrivalAirportId, const QString &flightNumber, int airlineId, int planeId) const
{
    QString queryStr = QString(queries::kCreateFlight)
                           .arg(departureDt.toString(util::sql::kSqlTimestampFormat))
                                    .arg(departureAirportId)
                                    .arg(arrivalDt.toString(util::sql::kSqlTimestampFormat))
                                    .arg(arrivalAirportId)
                                    .arg(flightNumber)
                                    .arg(airlineId)
                                    .arg(planeId);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return std::nullopt;
    query.next();
    return query.value(0).toBool();
}

std::optional<std::pair<int, int> > AirportDatabase::getAirportsByFlightId(int flightId) const
{
    QString queryStr = QString(queries::kGetAirportsByFlightId).arg(flightId);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 2))
        return std::nullopt;
    query.next();
    std::pair<int, int> res { query.value(0).toInt(), query.value(1).toInt() };
    return res;
}

std::optional<std::vector<AirportDatabase::PlaneSeatsPrice> > AirportDatabase::getPlaneSeatsByFlightId(int flightId) const
{
    QString queryStr = QString(queries::kGetPlaneSeats2).arg(flightId);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 6))
        return std::nullopt;
    std::vector<PlaneSeatsPrice> res;
    while (query.next())
    {
        res.emplace_back(query.value(0).toInt(), query.value(1).toInt(), query.value(2).toString(), query.value(3).toInt(), query.value(4).toInt(), query.value(5).toString());
    }
    return res;
}

std::optional<std::vector<AirportDatabase::AirlinesShort> > AirportDatabase::getAirlines() const
{
    QString queryStr = QString(queries::kGetAirlinesShort);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 3))
        return std::nullopt;
    std::vector<AirportDatabase::AirlinesShort> res;
    while (query.next())
    {
        res.emplace_back(query.value(0).toInt(), query.value(1).toString(), query.value(2).toString());
    }
    return res;
}

std::optional<std::vector<AirportDatabase::PlanesShort> > AirportDatabase::getPlanes() const
{
    QString queryStr = QString(queries::kGetPlanesShort);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 3))
        return std::nullopt;
    std::vector<AirportDatabase::PlanesShort> res;
    while (query.next())
    {
        res.emplace_back(query.value(0).toInt(), query.value(1).toString(), query.value(2).toString());
    }
    return res;
}

std::optional<std::vector<AirportDatabase::FlightDt> > AirportDatabase::getFlightsDtForPeriod(const QDateTime &beg, const QDateTime &end) const
{
    QString queryStr = QString(queries::kGetAllFlightsForPeriod)
            .arg(beg.toString(util::sql::kSqlTimestampFormat), end.toString(util::sql::kSqlTimestampFormat));
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 3))
        return std::nullopt;
    std::vector<AirportDatabase::FlightDt> res;
    while (query.next())
    {
        res.emplace_back(query.value(0).toInt(), query.value(1).toDateTime(), query.value(2).toDateTime());
    }
    return res;
}

std::optional<std::vector<AirportDatabase::FlightDt> > AirportDatabase::getFlightsDtForPeriod(const QDateTime &beg, const QDateTime &end, int departureAirportId, int arrivalAirportId) const
{
    QString queryStr = QString(queries::kGetFilteredFlightsForPeriod)
            .arg(beg.toString(util::sql::kSqlTimestampFormat))
            .arg(end.toString(util::sql::kSqlTimestampFormat))
            .arg(departureAirportId)
            .arg(arrivalAirportId);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 3))
        return std::nullopt;
    std::vector<AirportDatabase::FlightDt> res;
    while (query.next())
    {
        res.emplace_back(query.value(0).toInt(), query.value(1).toDateTime(), query.value(2).toDateTime());
    }
    return res;
}

std::optional<std::vector<AirportDatabase::DepartureCountryBirthYear> > AirportDatabase::getDepartureCountriesWithBirthYear() const
{
    QString queryStr(queries::kGetDepartureCountryWithBirthyear);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 2))
        return std::nullopt;
    std::vector<DepartureCountryBirthYear> res;
    while (query.next())
    {
        res.emplace_back(query.value(0).toInt(), query.value(1).toString());
    }
    return res;
}

std::optional<std::vector<AirportDatabase::FlightsSeatsBirthYear> > AirportDatabase::getSeatsToCountryStats(int birthYear, const QString &country) const
{
    QString queryStr = QString(queries::kGetSeatsToCountryStats).arg(birthYear).arg(country);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, -1, 4))
        return std::nullopt;
    std::vector<FlightsSeatsBirthYear> res;
    while (query.next())
    {
        res.emplace_back(query.value(1).toString(), query.value(2).toInt());
    }
    return res;
}

std::optional<bool> AirportDatabase::changePassword(int userId, const QString &oldPassword, const QString &newPassword) const
{
    QString hashedOldPassword = util::keccak256hash(oldPassword.toUtf8());
    QString hashedNewPassword = util::keccak256hash(newPassword.toUtf8());
    QString queryStr = QString(queries::kChangePassword).arg(userId).arg(hashedOldPassword).arg(hashedNewPassword);
    QSqlQuery query(getConnection());
    if (!executeQuery(query, queryStr, 1, 1))
        return std::nullopt;
    query.next();
    return query.value(0).toBool();
}


QSqlDatabase AirportDatabase::getUserConnection() const
{
    return QSqlDatabase::database("user_db");
}
