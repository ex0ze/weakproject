#include "airportmapwidgetsettings.hpp"

#include "config.hpp"
#include "logger.hpp"

void AirportMapWidgetSettings::loadSettings()
{
    auto& config = util::config();
    util::log()->debug("Loading config...");
    for (const auto& s : config.allKeys())
        util::log()->debug("Key: {} | Value: {}", s.toStdString(), config.value(s).toString().toStdString());
    mMapTheme = config.value("MapWidget/map_theme", "earth/plain/plain.dgml").toString();
    mPlaceIconScaleFactor = config.value("MapWidget/place_icon_scale_factor", 20).toInt();
    mPlaceLineColor = util::parse_color(config.value("MapWidget/place_line_color").toString()).value_or(QColor(255, 0, 0, 100));
    mPlaceLineWidth = config.value("MapWidget/place_line_width", 3).toInt();
    mTitleXOffset = config.value("MapWidget/title_x_offset", -5.0).toDouble();
    mTitleYOffset = config.value("MapWidget/title_y_offset", 30.0).toDouble();
    mTitleFontSize = config.value("MapWidget/title_font_size", 12).toInt();
    mTitleFontColor = util::parse_color(config.value("MapWidget/title_font_color").toString()).value_or(QColor(0, 0, 0, 255));
}

QString AirportMapWidgetSettings::mapTheme() const
{
    return mMapTheme;
}

int AirportMapWidgetSettings::placeIconScaleFactor() const
{
    return mPlaceIconScaleFactor;
}

QColor AirportMapWidgetSettings::placeLineColor() const
{
    return mPlaceLineColor;
}

int AirportMapWidgetSettings::placeLineWidth() const
{
    return mPlaceLineWidth;
}

int AirportMapWidgetSettings::titleXOffset() const
{
    return mTitleXOffset;
}

int AirportMapWidgetSettings::titleYOffset() const
{
    return mTitleYOffset;
}

int AirportMapWidgetSettings::titleFontSize() const
{
    return mTitleFontSize;
}

QColor AirportMapWidgetSettings::titleFontColor() const
{
    return mTitleFontColor;
}
