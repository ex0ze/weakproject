#include "loginregisterdialog.hpp"
#include "ui_loginregisterdialog.h"
#include <QMessageBox>
#include "airportdatabase.hpp"

LoginRegisterDialog::LoginRegisterDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginRegisterDialog)
{
    ui->setupUi(this);
    ui->birthDateEdit->setCalendarPopup(true);
    ui->birthDateEdit->setMaximumDate(QDateTime::currentDateTime().date());
    connect(ui->showPassCheckbox, &QCheckBox::stateChanged, [this](int state) {
        QLineEdit::EchoMode mode;
        if (state == Qt::CheckState::Unchecked)
            mode = QLineEdit::EchoMode::Password;
        else
            mode = QLineEdit::EchoMode::Normal;
        ui->tabLoginPassLineEdit->setEchoMode(mode);
    });
    connect(ui->showPassCheckbox_2, &QCheckBox::stateChanged, [this](int state) {
        QLineEdit::EchoMode mode;
        if (state == Qt::CheckState::Unchecked)
            mode = QLineEdit::EchoMode::Password;
        else
            mode = QLineEdit::EchoMode::Normal;
        ui->tabPassPassLineEdit->setEchoMode(mode);
        ui->tabPassPassLineEdit2->setEchoMode(mode);
    });
    auto updateStatus = [this]()
    {
        bool passwordsAreSame = ui->tabPassPassLineEdit->text() == ui->tabPassPassLineEdit2->text();
        if (passwordsAreSame && ui->tabPassPassLineEdit->text().isEmpty() && ui->tabPassPassLineEdit2->text().isEmpty())
        {
            ui->statusLabel->setText("");
            return ;
        }
        auto palette = ui->statusLabel->palette();
        palette.setColor(ui->statusLabel->backgroundRole(), passwordsAreSame ? Qt::green : Qt::red);
        palette.setColor(ui->statusLabel->foregroundRole(), passwordsAreSame ? Qt::green : Qt::red);
        ui->statusLabel->setPalette(palette);
        ui->statusLabel->setText(passwordsAreSame ? "Пароли совпадают" : "Пароли не совпадают");
    };
    connect(ui->tabPassPassLineEdit, &QLineEdit::textEdited, updateStatus);
    connect(ui->tabPassPassLineEdit2, &QLineEdit::textEdited, updateStatus);
    connect(ui->loginEnterBtn, &QPushButton::clicked, this, &LoginRegisterDialog::onLoginClicked);
    connect(ui->registerBtn, &QPushButton::clicked, this, &LoginRegisterDialog::onRegisterClicked);
}

void LoginRegisterDialog::onLoginClicked()
{
    if (ui->tabLoginLoginLineEdit->text().isEmpty() || ui->tabLoginPassLineEdit->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка входа", "Заполните поля 'Логин' и 'Пароль'");
        return;
    }
    std::optional<bool> maybeRes = AirportDatabase::instance().login(ui->tabLoginLoginLineEdit->text(), ui->tabLoginPassLineEdit->text());
    if (!maybeRes.has_value())
    {
        QMessageBox::critical(this, "Ошибка входа", "Произошла системная ошибка, проверьте логи");
        return;
    }
    bool res = maybeRes.value();
    if (res)
    {
        const auto& user = AirportDatabase::instance().getCurrentUser();
        QMessageBox::information(this, "Успешный вход", QString("Добро пожаловать, %1\nВы вошли в систему как %2")
                                                            .arg(user.fullName())
                                                            .arg(AirportUser::stringForPrivileges(user.privileges())));
        close();
        emit successfullyAuthorized();
    }
    else
    {
        QMessageBox::critical(this, "Ошибка входа", "Неверная пара логин-пароль");
    }
}

void LoginRegisterDialog::onRegisterClicked()
{
    QString login = ui->tabPassloginLineEdit->text();
    QString password1 = ui->tabPassPassLineEdit->text();
    QString password2 = ui->tabPassPassLineEdit2->text();
    QDate birthdate = ui->birthDateEdit->date();
    QString fullname = ui->fullNameEdit->text();

    if (login.isEmpty())
    {
        QMessageBox::critical(this, "Ошибка входа", "Заполните поле логина");
        return;
    }
    if (password1.isEmpty() || password2.isEmpty())
    {
        QMessageBox::critical(this, "Ошибка входа", "Заполните поля пароля");
        return;
    }
    if (password1 != password2)
    {
        QMessageBox::critical(this, "Ошибка входа", "Пароли не сопадают");
        return;
    }
    if (fullname.isEmpty())
    {
        QMessageBox::critical(this, "Ошибка входа", "Введите ФИО");
        return;
    }
    AirportUser user;
    user.setPrivileges(AirportUser::Privileges::User);
    user.setUserName(login);
    user.setPassword(password1);
    user.setBirthDate(birthdate);
    user.setFullName(fullname);
    std::optional<bool> maybeRes = AirportDatabase::instance().reg(std::move(user));
    if (!maybeRes.has_value())
    {
        QMessageBox::critical(this, "Ошибка входа", "Произошла системная ошибка, проверьте логи");
        return;
    }
    bool res = maybeRes.value();
    if (res)
    {
        const auto& user = AirportDatabase::instance().getCurrentUser();
        QMessageBox::information(this, "Успешная регистрация", QString("Добро пожаловать, %1\nВы зарегистрировались в систему как %2")
                                                            .arg(user.fullName())
                                                            .arg(AirportUser::stringForPrivileges(user.privileges())));
        close();
        emit successfullyAuthorized();
    }
    else
    {
        QMessageBox::critical(this, "Ошибка регистрации", "Пользователь с таким логином уже существует");
    }
}

LoginRegisterDialog::~LoginRegisterDialog()
{
    delete ui;
}
