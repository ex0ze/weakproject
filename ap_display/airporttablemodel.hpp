#pragma once

#include <QAbstractTableModel>
#include "airportprovider.hpp"

// класс модели для таблиц с аэропортами, отображает глобальную модель аэропортов на табличное представление
class AirportTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    AirportTableModel(QObject * parent = nullptr);
    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &) const override;
    virtual int columnCount(const QModelIndex &) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
};

Q_DECLARE_METATYPE(Airport*)
