#include "infographicwidget.hpp"
#include "ui_infographicwidget.h"
#include <QMessageBox>
#include "airporttablemodel.hpp"
#include "sqlutils.hpp"
#include "airportdatabase.hpp"

InfographicWidget::InfographicWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfographicWidget)
{
    ui->setupUi(this);
    ui->updateBtn->setIcon(QIcon(":/update.png"));
    initFlightsTab();
    initSeatsTab();
}

InfographicWidget::~InfographicWidget()
{
    delete ui;
}

void InfographicWidget::initFlightsTab()
{
    pFlightsCountChart = new FlightsCountChartView;
    ui->flightsGraphView->addWidget(pFlightsCountChart);
    util::sql::setupAirportsTable(ui->airportsView);
    connect(ui->setDepartureApBtn, &QPushButton::clicked, [this] {
        auto table = ui->airportsView;
        auto currentIdx = table->currentIndex();
        if (!currentIdx.isValid())
            return;
        Airport * airport = currentIdx.data(Qt::UserRole).value<Airport*>();
        mFlightsSelectedAirports.first = airport;
        ui->departureApLabel->setText(QString("%1 (%2)").arg(airport->mName, airport->mCountry));
    });
    connect(ui->setArrivalApBtn, &QPushButton::clicked, [this] {
        auto table = ui->airportsView;
        auto currentIdx = table->currentIndex();
        if (!currentIdx.isValid())
            return;
        Airport * airport = currentIdx.data(Qt::UserRole).value<Airport*>();
        mFlightsSelectedAirports.second = airport;
        ui->arrivalApLabel->setText(QString("%1 (%2)").arg(airport->mName, airport->mCountry));
    });
    connect(ui->clearDepartureApBtn, &QPushButton::clicked, [this] {
        mFlightsSelectedAirports.first = nullptr;
        ui->departureApLabel->clear();
    });
    connect(ui->clearArrivalApBtn, &QPushButton::clicked, [this] {
        mFlightsSelectedAirports.second = nullptr;
        ui->arrivalApLabel->clear();
    });
    connect(ui->showFlightsGraphBtn, &QPushButton::clicked, [this] {
        if ((!mFlightsSelectedAirports.first && mFlightsSelectedAirports.second) || (mFlightsSelectedAirports.first && !mFlightsSelectedAirports.second))
        {
            QMessageBox::critical(this, "Ошибка", "Выберите либо два аэропорта, либо не выберите ни одного");
            return;
        }
        auto beginDt = ui->startPeriodDtEdit->dateTime();
        auto endDt = ui->endPeriodDtEdit->dateTime();
        if (beginDt >= endDt)
        {
            QMessageBox::critical(this, "Ошибка", "Дата начала периода должна быть меньше даты конца периода");
            return;
        }
        std::optional<std::vector<AirportDatabase::FlightDt>> maybeRes;
        if (!mFlightsSelectedAirports.first && !mFlightsSelectedAirports.second)
            maybeRes = AirportDatabase::instance().getFlightsDtForPeriod(beginDt, endDt);
        else
            maybeRes = AirportDatabase::instance().getFlightsDtForPeriod(beginDt, endDt, mFlightsSelectedAirports.first->mId, mFlightsSelectedAirports.second->mId);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка", "Произошла системная ошибка, проверьте логи");
            return;
        }
        pFlightsCountChart->setFlights(std::move(maybeRes.value()));
    });
}

void InfographicWidget::initSeatsTab()
{
    pSeatsCountriesChart = new SeatsCountriesCharView;
    ui->countriesFlightsLayout->addWidget(pSeatsCountriesChart);
    connect(ui->birthYearCombobox, &QComboBox::currentTextChanged, [this](const QString& str) {
        bool ok = true;
        int num = str.toInt(&ok);
        if (!ok)
            return;
        ui->depCountryCombobox->clear();
        for (const auto& country : mSeatsComboboxData[num])
            ui->depCountryCombobox->addItem(country);
    });
    connect(ui->updateBtn, &QPushButton::clicked, this, &InfographicWidget::updateSeatsTab);
    connect(ui->showCountriesStatsBtn, &QPushButton::clicked, [this] {
        if (ui->birthYearCombobox->currentIndex() == -1)
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Выберите пожалуйста год");
            return ;
        }
        if (ui->depCountryCombobox->currentIndex() == -1)
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Выберите пожалуйста Страну");
            return ;
        }
        int year = ui->birthYearCombobox->currentText().toInt();
        QString country = ui->depCountryCombobox->currentText();
        auto maybeRes = AirportDatabase::instance().getSeatsToCountryStats(year, country);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка", "Произошла системная ошибка, проверьте логи");
            return;
        }
        auto res = std::move(maybeRes.value());
        pSeatsCountriesChart->setSeatsToCountryStats(std::move(res));
    });
    updateSeatsTab();
}

void InfographicWidget::updateSeatsTab()
{
    auto maybeRes = AirportDatabase::instance().getDepartureCountriesWithBirthYear();
    if (!maybeRes.has_value())
    {
        QMessageBox::critical(this, "Ошибка обновления", "Ошибка обновления инфографиков, попробуйте позже");
        util::log()->critical("Ошибка обновления инфографиков {} {}", __FILE__, __func__);
        return;
    }
    auto res = std::move(maybeRes.value());
    mSeatsComboboxData.clear();
    for (auto& [year, country] : res)
    {
        mSeatsComboboxData[year].insert(std::move(country));
    }
    ui->birthYearCombobox->clear();
    for (const auto & i : mSeatsComboboxData)
        ui->birthYearCombobox->addItem(QString::number(i.first));
}
