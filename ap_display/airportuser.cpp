#include "airportuser.hpp"



std::optional<AirportUser::Privileges> AirportUser::privilegesForNumber(int val)
{
    if (val <= 0 || val > 2)
        return std::nullopt;
    return static_cast<Privileges>(val);
}

std::optional<int> AirportUser::intForPrivileges(AirportUser::Privileges p)
{
    int res = static_cast<int>(p);
    if (res <= 0 || res > 2)
        return std::nullopt;
    return res;
}

bool AirportUser::isAuthorized() const
{
    return mIsAuthorized;
}


void AirportUser::setIsAuthorized(bool isAuthorized)
{
    mIsAuthorized = isAuthorized;
}

AirportUser::Privileges AirportUser::privileges() const
{
    return mPrivileges;
}

void AirportUser::setPrivileges(const AirportUser::Privileges &privileges)
{
    mPrivileges = privileges;
}

QString AirportUser::userName() const
{
    return mUserName;
}

void AirportUser::setUserName(const QString &userName)
{
    mUserName = userName;
}

QString AirportUser::fullName() const
{
    return mFullName;
}

QDate AirportUser::birthDate() const
{
    return mBirthDate;
}

void AirportUser::setBirthDate(const QDate &birthDate)
{
    mBirthDate = birthDate;
}

QString AirportUser::password() const
{
    return mPassword;
}

void AirportUser::setPassword(const QString &password)
{
    mPassword = password;
}

int AirportUser::id() const
{
    return mId;
}

void AirportUser::setId(int id)
{
    mId = id;
}

void AirportUser::setFullName(const QString &fullName)
{
    mFullName = fullName;
}
