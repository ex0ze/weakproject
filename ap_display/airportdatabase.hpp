#pragma once
#include <QSqlDatabase>
#include "logger.hpp"
#include "airportuser.hpp"
#include "macro.hpp"
#include <optional>
#include <tuple>
#include <map>
//класс связи с базой данных, представляет максимально простой интерфейс для данной предметной области
class AirportDatabase
{
    DISABLE_COPY_MOVE(AirportDatabase);
public:
    using PlaneTypes = std::map<int, QString>;
    using SeatTypes = std::map<int, QString>;
    // id name legal_address
    using AirlinesShort = std::tuple<int, QString, QString>;
    // id name tail_number
    using PlanesShort = std::tuple<int, QString, QString>;
    // id рейса plane_seat_id тип места количество забронировавших количество всего цена
    using PlaneSeatsPrice = std::tuple<int, int, QString, int, int, QString>;
    // id рейса дата и время отправки дата и время прибытия
    using FlightDt = std::tuple<int, QDateTime, QDateTime>;
    // год рождения - страна вылета
    using DepartureCountryBirthYear = std::pair<int, QString>;
    // тип места на количество
    using FlightsSeatsBirthYear = std::pair<QString, int>;

    static AirportDatabase& instance();
    bool init();
    AirportUser& getCurrentUser() noexcept;
    const AirportUser& getCurrentUser() const noexcept;
    std::optional<bool> login(const QString& username, const QString& password);
    std::optional<bool> reg(AirportUser newUser);
    QSqlDatabase getConnection() const;
    std::optional<PlaneTypes> getPlaneTypes() const;
    std::optional<SeatTypes> getSeatTypes() const;
    std::optional<bool> createPlane(const QString& name, const QString& tailNumber, int idType) const;
    std::optional<bool> addPlaneSeats(int planeId, int seatTypeId, int seatsCount) const;
    std::optional<bool> bookTicket(int userId, int flightId, int planeSeatId, int piecesOfBaggage) const;
    std::optional<bool> createAirlines(const QString& name, const QString& legalAddress);
    std::optional<bool> createFlight(const QDateTime& departureDt, const QDateTime& arrivalDt,
        int departureAirportId, int arrivalAirportId,
        const QString& flightNumber, int airlineId, int planeId) const;
    std::optional<std::pair<int, int>> getAirportsByFlightId(int flightId) const;
    std::optional<std::vector<PlaneSeatsPrice>> getPlaneSeatsByFlightId(int flightId) const;
    std::optional<std::vector<AirlinesShort>> getAirlines() const;
    std::optional<std::vector<PlanesShort>> getPlanes() const;
    std::optional<std::vector<FlightDt>> getFlightsDtForPeriod(const QDateTime& beg, const QDateTime& end) const;
    std::optional<std::vector<FlightDt>> getFlightsDtForPeriod(const QDateTime& beg, const QDateTime& end, int departureAirportId, int arrivalAirportId) const;
    std::optional<std::vector<DepartureCountryBirthYear>> getDepartureCountriesWithBirthYear() const;
    std::optional<std::vector<FlightsSeatsBirthYear>> getSeatsToCountryStats(int birthYear, const QString& country) const;
    std::optional<bool> changePassword(int userId, const QString& oldPassword, const QString& newPassword) const;

private:
    AirportDatabase() = default;
    QSqlDatabase getUserConnection() const;
    QSqlDatabase getAdminConnection() const;
    AirportUser     mCurrentUser;

    bool executeQuery(QSqlQuery& query, const QString& queryStr, int numRowsExpected = -1, int numColsExpected = -1) const;
    void obtainUserId();

};



