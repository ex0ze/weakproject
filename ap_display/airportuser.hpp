#pragma once
#include <QString>
#include <QDate>
#include <optional>

// класс текущего пользователя в системе
class AirportUser
{
public:
    enum class Privileges
    {
        Undefined   = 0,
        User        = 1,
        Admin       = 2
    };
    AirportUser() = default;
    AirportUser(AirportUser&&) = default;
    AirportUser(const AirportUser&) = default;
    AirportUser& operator=(AirportUser&&) = default;
    AirportUser& operator=(const AirportUser&) = default;

    static std::optional<Privileges> privilegesForNumber(int val);
    static QString stringForPrivileges(AirportUser::Privileges p)
    {
        using pr = Privileges;
        switch (p) {
        case pr::User:
            return "пользователь";
        case pr::Admin:
            return "администратор";
        case pr::Undefined:
        default:
            return "неизвестно";
        }
    }
    static std::optional<int> intForPrivileges(AirportUser::Privileges p);

    bool isAuthorized() const;
    void setIsAuthorized(bool isAuthorized);

    Privileges privileges() const;
    void setPrivileges(const Privileges &privileges);

    QString userName() const;
    void setUserName(const QString &userName);

    QString fullName() const;
    void setFullName(const QString &fullName);

    QDate birthDate() const;
    void setBirthDate(const QDate &birthDate);

    QString password() const;
    void setPassword(const QString &password);



    int id() const;
    void setId(int id);

private:
    bool        mIsAuthorized {false};
    Privileges  mPrivileges {Privileges::Undefined};
    QString     mUserName;
    QString     mFullName;
    int         mId = -1;
    // только для регистрации
    QString     mPassword;
    QDate       mBirthDate;
};










