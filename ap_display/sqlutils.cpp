#include "sqlutils.hpp"


#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QTableView>
#include <QSortFilterProxyModel>
#include "logger.hpp"
#include "airportdatabase.hpp"
#include "airporttablemodel.hpp"


void util::sql::setReadonlyModel(QSqlDatabase db, QTableView *view, const QString &table, const std::set<int> &columnsToHide, const QString &filter)
{
    QSqlTableModel * model = new QSqlTableModel(view, db);
    model->setTable(table);
    model->setFilter(filter);
    if (!model->select())
        util::log()->critical("Error while select from sqltablemodel: {} ({} {})", model->lastError().text().toStdString(), __FILE__, __func__);
    view->setModel(model);
    view->setEditTriggers(QTableView::NoEditTriggers);
    view->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    view->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    view->resizeColumnsToContents();
    for (auto col : columnsToHide)
        view->hideColumn(col);
}

void util::sql::updateSqlTableBasedModel(QTableView *view)
{
    auto model = dynamic_cast<QSqlTableModel*>(view->model());
    if (!model)
    {
        util::log()->critical("Error dynamic_cast: wrong type of model({} {})", __FILE__, __func__);
        return;
    }
    if (!model->select())
        util::log()->critical("Error while select from sqltablemodel: {} ({} {})", model->lastError().text().toStdString(), __FILE__, __func__);
    view->resizeColumnsToContents();
}

void util::sql::updateSqlQueryBasedModel(QTableView *view)
{
    auto model = dynamic_cast<QSqlQueryModel*>(view->model());
    if (!model)
    {
        util::log()->critical("Error dynamic_cast: wrong type of model({} {})", __FILE__, __func__);
        return;
    }
    model->setQuery(model->query().lastQuery(), AirportDatabase::instance().getConnection());
    view->resizeColumnsToContents();
}

void util::sql::setReadWriteTableModel(QSqlDatabase db, QTableView *view, const QString &table, const std::set<int> &columnsToHide, const QString& filter)
{
    QSqlTableModel * model;
    bool isNewModel = false;
    if (view->model())
    {
        model = dynamic_cast<QSqlTableModel*>(view->model());
        if (!model)
        {
            util::log()->critical("Error dynamic_cast: wrong type of model({} {})", __FILE__, __func__);
            return;
        }
    }
    else
    {
        isNewModel = true;
        model = new QSqlTableModel(view, db);
    }
    model->setTable(table);
    model->setFilter(filter);
    if (!model->select())
        util::log()->critical("Error while select from sqltablemodel: {} ({} {})", model->lastError().text().toStdString(), __FILE__, __func__);
    if (isNewModel)
        view->setModel(model);
    view->resizeColumnsToContents();
    for (auto col : columnsToHide)
        view->hideColumn(col);
    view->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
}

void util::sql::setReadWriteQueryModel(QSqlDatabase db, QTableView *view, const QString &query, const std::set<int> &columnsToHide)
{
    if (view->model())
        delete view->model();
    QSqlQueryModel * model = new QSqlQueryModel;
    model->setQuery(query, db);
    view->setModel(model);
    for (int col : columnsToHide)
        view->hideColumn(col);
    view->resizeColumnsToContents();
    view->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
}

void util::sql::setReadonlyQueryBasedModel(QSqlDatabase db, QTableView *view, const QString &query, const std::set<int> &columnsToHide)
{
    if (view->model())
        delete view->model();
    auto model = new QSqlQueryModel;
    model->setQuery(query, db);
    auto err = model->lastError();
    if (err.type() != QSqlError::ErrorType::NoError)
    {
        util::log()->critical("Error while select from sqlquerymodel: {} ({} {})", model->lastError().text().toStdString(), __FILE__, __func__);
    }
    view->setModel(model);
    view->setEditTriggers(QTableView::NoEditTriggers);
    view->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    view->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    view->resizeColumnsToContents();
    for (auto col : columnsToHide)
        view->hideColumn(col);


}

void util::sql::setupAirportsTable(QTableView *view)
{
    auto apModel = new AirportTableModel(view);
    QSortFilterProxyModel * proxyModel = new QSortFilterProxyModel(view);
    proxyModel->setSourceModel(apModel);
    view->setSortingEnabled(true);
    view->setModel(proxyModel);
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
}
