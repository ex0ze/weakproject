#pragma once

#include <QWidget>

namespace Ui {
class LoginRegisterDialog;
}
//класс диалога авторизации или входа в систему, показывающийся при старте программы
class LoginRegisterDialog : public QWidget
{
    Q_OBJECT

public:
    explicit LoginRegisterDialog(QWidget *parent = nullptr);
    ~LoginRegisterDialog();
signals:
    void successfullyAuthorized();
private:
    Ui::LoginRegisterDialog *ui;
private slots:
    void onLoginClicked();
    void onRegisterClicked();
};

