#include "adminwidget.hpp"
#include "ui_adminwidget.h"
#include <QMessageBox>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSortFilterProxyModel>
#include <QHeaderView>
#include <QSqlError>
#include <marble/GeoDataDocument.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoDataTreeModel.h>
#include <marble/MarbleModel.h>
#include <marble/AbstractFloatItem.h>
#include "noeditablecolumndelegate.hpp"
#include "sqlutils.hpp"
#include "airportdatabasequeries.hpp"
#include "airportprovider.hpp"
#include "airporttablemodel.hpp"
#include "infographicwidget.hpp"

AdminWidget::AdminWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminWidget)
{
    ui->setupUi(this);
    auto maybePlaneTypes = AirportDatabase::instance().getPlaneTypes();
    if (maybePlaneTypes.has_value())
    {
        mPlaneTypes = std::move(maybePlaneTypes.value());
        for (const auto& tp : mPlaneTypes)
            ui->planeTypeCombobox->addItem(tp.second, tp.first);
        if (mPlaneTypes.size() > 0)
            ui->planeTypeCombobox->setCurrentIndex(0);
    }
    auto maybeSeatTypes = AirportDatabase::instance().getSeatTypes();
    if (maybeSeatTypes.has_value())
    {
        mSeatTypes = std::move(maybeSeatTypes.value());
        for (const auto& tp : mSeatTypes)
            ui->seatTypeCombobox->addItem(tp.second, tp.first);
        if (mSeatTypes.size() > 0)
            ui->seatTypeCombobox->setCurrentIndex(0);
    }
    initConnections();
    initTables();
    pInfographicWidget = new InfographicWidget;
    ui->infoGraphsLayout->addWidget(pInfographicWidget);
}

void AdminWidget::initConnections()
{
    connect(ui->addPlaneBtn, &QPushButton::clicked, [this] {
        QString name = ui->planeNameEdit->text();
        QString tailNumber = ui->planeNumberEdit->text();
        if (name.isEmpty() || tailNumber.isEmpty())
        {
            QMessageBox::critical(this, "Ошибка", "Заполните все поля пожалуйста");
            return ;
        }
        if (!ui->planeTypeCombobox->currentData().isValid())
        {
            QMessageBox::critical(this, "Ошибка", "Выберите тип самолёта");
            return;
        }
        int typeId = ui->planeTypeCombobox->currentData().toInt();
        auto maybeRes = AirportDatabase::instance().createPlane(name, tailNumber, typeId);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Произошла системная ошибка, проверьте логи");
            return;
        }
        bool res = maybeRes.value();
        if (res)
        {
            updatePlaneModel();
            emit planesUpdated();
            QMessageBox::information(this, "Успешно", "Самолёт добавлен");
        }
        else
        {
            QMessageBox::critical(this, "Ошибка добавления", "Самолёт с таким бортовым номером уже существует");
        }
    });

    connect(ui->planesView, &QTableView::clicked, [this](const QModelIndex& index) {
        auto oldModel = ui->planeSeatsView->model();
        if (oldModel)
            delete oldModel;
        int row = index.row();
        QString planeName = ui->planesView->model()->index(row, 1).data().toString();
        ui->selectedPlaneLabel->setText(planeName);
        int planeId = ui->planesView->model()->index(row, 0).data().toInt();
        mCurrentPlaneId = planeId;
        auto model = new QSqlQueryModel(nullptr);
        model->setQuery(QString(queries::kGetPlaneSeats).arg(planeId), AirportDatabase::instance().getConnection());
        ui->planeSeatsView->setModel(model);
        ui->planeSeatsView->hideColumn(0);
        ui->planeSeatsView->resizeColumnsToContents();
    });

    connect(ui->addSeatsBtn, &QPushButton::clicked, [this] {
        if (mCurrentPlaneId == -1)
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Выберите пожалуйста самолёт для добавления мест");
            return;
        }
        if (ui->seatTypeCombobox->currentIndex() == -1)
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Выберите пожалуйста тип мест");
            return;
        }
        int seatsCount = ui->seatsCountSpinbox->value();
        int seatTypeId = ui->seatTypeCombobox->currentData().toInt();
        auto maybeRes = AirportDatabase::instance().addPlaneSeats(mCurrentPlaneId, seatTypeId, seatsCount);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Произошла системная ошибка, проверьте логи");
            return;
        }
        bool res = maybeRes.value();
        if (res)
        {
            util::sql::updateSqlQueryBasedModel(ui->planeSeatsView);
            QMessageBox::information(this, "Успешно", "Места добавлены");
        }
        else
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Ошибка при добавлении мест");
            return;
        }
    });

    connect(ui->airlineAddBtn, &QPushButton::clicked, [this] {
        QString name = ui->airlineNameEdit->text();
        QString address = ui->airlineAddressEdit->text();
        if (name.isEmpty() || address.isEmpty())
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Введите пожалуйста название и юр. адрес авиалинии");
            return;
        }
        auto maybeRes = AirportDatabase::instance().createAirlines(name, address);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Произошла системная ошибка, проверьте логи");
            return;
        }
        bool res = maybeRes.value();
        if (res)
        {
            updateAirlinesView();
            QMessageBox::information(this, "Успешно", "Авиалиния добавлена");
            emit airlinesUpdated();
        }
        else
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Ошибка при добавлении авиалинии");
            return;
        }
    });
}

void AdminWidget::initTables()
{
    initPlanesTab();
    initAirlinesTab();
    initFlightsTab();
    initFlightsEditTab();
}

void AdminWidget::initAirlinesTab()
{
    util::sql::setReadWriteTableModel(AirportDatabase::instance().getConnection(), ui->airlinesView, "airlines", {0});
    ui->airlinesView->hideColumn(0);
    ui->airlinesView->model()->setHeaderData(1, Qt::Orientation::Horizontal, QString("Авиалиния"), Qt::DisplayRole);
    ui->airlinesView->model()->setHeaderData(2, Qt::Orientation::Horizontal, QString("Адрес"), Qt::DisplayRole);
    ui->airlinesView->resizeColumnsToContents();
}

void AdminWidget::initPlanesTab()
{
    util::sql::setReadonlyModel(AirportDatabase::instance().getConnection(), ui->planesView, "detailed_planes", {0});
    ui->planeSeatsView->setEditTriggers(QTableView::NoEditTriggers);
    ui->planeSeatsView->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    ui->planeSeatsView->hideColumn(0);
}

void AdminWidget::initFlightsTab()
{
    pMapWidget = new AirportMapWidget;
    ui->flightMapLayout->addWidget(pMapWidget);
    ui->flightDepartureDt->setDateTime(QDateTime::currentDateTime());
    ui->flightArrivalDt->setDateTime(QDateTime::currentDateTime());

    auto fillAirlinesCombobox = [this] {
        auto maybeAirlines = AirportDatabase::instance().getAirlines();
        if (!maybeAirlines.has_value())
        {
            return;
        }
        const auto airlines = std::move(maybeAirlines.value());
        // сохраняем старый выбор, если он конечно был
        int oldSelection;
        if (auto data = ui->flightAirlineCombobox->currentData(); data.isValid())
            oldSelection = data.toInt();
        else
            oldSelection = -1;
        ui->flightAirlineCombobox->clear();
        int currentItemId = 0;
        for (const auto& [id, name, legalAddress] : airlines)
        {
            ui->flightAirlineCombobox->addItem(QString("%1 (%2)").arg(name).arg(legalAddress), id);
            if (oldSelection != -1 && id == oldSelection)
                ui->flightAirlineCombobox->setCurrentIndex(currentItemId);
            ++currentItemId;
        }
    };
    fillAirlinesCombobox();
    connect(this, &AdminWidget::airlinesUpdated, fillAirlinesCombobox);

    auto fillPlanesCombobox = [this] {
        auto maybePlanes = AirportDatabase::instance().getPlanes();
        if (!maybePlanes.has_value())
        {
            return;
        }
        const auto airlines = std::move(maybePlanes.value());
        // сохраняем старый выбор, если он конечно был
        int oldSelection;
        if (auto data = ui->flightPlaneCombobox->currentData(); data.isValid())
            oldSelection = data.toInt();
        else
            oldSelection = -1;
        ui->flightPlaneCombobox->clear();
        int currentItemId = 0;
        for (const auto& [id, name, tailNumber] : airlines)
        {
            ui->flightPlaneCombobox->addItem(QString("%1 (%2)").arg(name).arg(tailNumber), id);
            if (oldSelection != -1 && id == oldSelection)
                ui->flightPlaneCombobox->setCurrentIndex(currentItemId);
            ++currentItemId;
        }
    };
    fillPlanesCombobox();
    connect(this, &AdminWidget::planesUpdated, fillPlanesCombobox);
    fillFlightsAirportsTable();
    connect(ui->flightSetDepartureBtn, &QPushButton::clicked, [this] {
        auto table = ui->flightAirportsTable;
        auto currentIdx = table->currentIndex();
        if (!currentIdx.isValid())
            return;
        Airport * airport = currentIdx.data(Qt::UserRole).value<Airport*>();
        auto& currentSelectedAirports = mSelectedAirports[ui->flightsTabWidget->currentIndex()];
        ui->flightDepartureAPLabel->setText(QString("%1 (%2)").arg(airport->mName).arg(airport->mCountry));
        currentSelectedAirports.first = airport;
        emit airportSelected(airport, SelectedAirportType::Departure);

    });
    connect(ui->flightSetArrivalBtn, &QPushButton::clicked, [this] {
        auto table = ui->flightAirportsTable;
        auto currentIdx = table->currentIndex();
        if (!currentIdx.isValid())
            return;
        Airport * airport = currentIdx.data(Qt::UserRole).value<Airport*>();
        auto& currentSelectedAirports = mSelectedAirports[ui->flightsTabWidget->currentIndex()];
        ui->flightArrivalAPLabel->setText(QString("%1 (%2)").arg(airport->mName).arg(airport->mCountry));
        currentSelectedAirports.second = airport;
        emit airportSelected(airport, SelectedAirportType::Arrival);
    });
    connect(this, &AdminWidget::airportSelected, this, &AdminWidget::drawAirportOnMap);
    connect(ui->flightAddBtn, &QPushButton::clicked, [this] {
        QDateTime departureDt = ui->flightDepartureDt->dateTime();
        QDateTime arrivalDt = ui->flightArrivalDt->dateTime();
        if (departureDt >= arrivalDt)
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Дата и время вылета должны быть меньше даты и времени прибытия");
            return ;
        }
        QString flightNumber = ui->flightNumberEdit->text();
        if (flightNumber.isEmpty())
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Введите пожалуйста номер рейса");
            return ;
        }
        int airlineId;
        if (auto data = ui->flightAirlineCombobox->currentData(); data.isValid())
            airlineId = data.toInt();
        else
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Введите пожалуйста авиалинию");
            return ;
        }
        int planeId;
        if (auto data = ui->flightPlaneCombobox->currentData(); data.isValid())
            planeId = data.toInt();
        else
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Введите пожалуйста самолёт");
            return ;
        }
        auto& currentSelectedAirports = mSelectedAirports[ui->flightsTabWidget->currentIndex()];
        if (!currentSelectedAirports.first)
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Введите пожалуйста аэропорт вылета");
            return ;
        }
        if (!currentSelectedAirports.second)
        {
            QMessageBox::critical(this, "Ошибка заполнения", "Введите пожалуйста аэропорт прибытия");
            return ;
        }
        int departureAirportId = currentSelectedAirports.first->mId;
        int arrivalAirportId = currentSelectedAirports.second->mId;
        auto maybeRes = AirportDatabase::instance().createFlight(departureDt, arrivalDt, departureAirportId, arrivalAirportId, flightNumber, airlineId, planeId);
        if (!maybeRes.has_value())
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Произошла системная ошибка, проверьте логи");
            return;
        }
        bool res = maybeRes.value();
        if (res)
        {
            updateAirlinesView();
            QMessageBox::information(this, "Успешно", "Рейс добавлен");
            emit airlinesUpdated();
            emit flightsUpdated();
        }
        else
        {
            QMessageBox::critical(this, "Ошибка Добавления", "Ошибка при добавлении рейса");
            return;
        }
    });
}

void AdminWidget::initFlightsEditTab()
{
    util::sql::setReadonlyModel(AirportDatabase::instance().getConnection(), ui->flightsView, "detailed_flights", {0});
    ui->flightsView->setSortingEnabled(true);
    connect(this, &AdminWidget::flightsUpdated, [this] {
        util::sql::updateSqlTableBasedModel(ui->flightsView);
    });
    connect(ui->flightsView, &QTableView::clicked, [this](const QModelIndex& index) {
        int row = index.row();
        int flightId = ui->flightsView->model()->index(row, 0).data().toInt();
        const std::pair<int, int> airportsId = AirportDatabase::instance().getAirportsByFlightId(flightId).value_or(std::make_pair(-1, -1));
        if (airportsId.first == -1 && airportsId.second == -1)
            return;
        auto& currentAirports = mSelectedAirports[ui->flightsTabWidget->currentIndex()];
        currentAirports.first = AirportProvider::instance().getAirport(airportsId.first);
        currentAirports.second = AirportProvider::instance().getAirport(airportsId.second);
        drawAirportOnMap(nullptr, SelectedAirportType::Both);
        util::sql::setReadWriteTableModel(AirportDatabase::instance().getConnection(),
                                          ui->flightsPricesView,
                                          "detailed_plane_seats_prices",
                                          {0, 1},
                                          QString("flight_id = %1").arg(flightId));
        for (int c : {2, 3})
        {
            ui->flightsPricesView->setItemDelegateForColumn(c, new NotEditableDelegate(ui->flightsPricesView));
        }
    });
    connect(ui->flightsTabWidget, &QTabWidget::currentChanged, [this] {
        drawAirportOnMap(nullptr, SelectedAirportType::Both);
    });
}

void AdminWidget::fillFlightsAirportsTable()
{
    util::sql::setupAirportsTable(ui->flightAirportsTable);
}

void AdminWidget::drawAirportOnMap(Airport *apPtr, AdminWidget::SelectedAirportType apType)
{
    auto& currentSelectedAirports = mSelectedAirports[ui->flightsTabWidget->currentIndex()];
    if (apType == SelectedAirportType::Departure)
        currentSelectedAirports.first = apPtr;
    else if (apType == SelectedAirportType::Arrival)
        currentSelectedAirports.second = apPtr;
    pMapWidget->setConnectAirports(true);
    pMapWidget->setAirports({ {currentSelectedAirports.first, "Точка отправки"}, {currentSelectedAirports.second, "Точка прибытия"} });
    pMapWidget->update();
}

AdminWidget::~AdminWidget()
{
    delete ui;
}

void AdminWidget::updatePlaneModel()
{
    util::sql::updateSqlTableBasedModel(ui->planesView);
}

void AdminWidget::updateAirlinesView()
{
    util::sql::updateSqlTableBasedModel(ui->airlinesView);
}
