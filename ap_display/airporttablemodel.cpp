#include "airporttablemodel.hpp"

AirportTableModel::AirportTableModel(QObject *parent) : QAbstractTableModel(parent) {}

int AirportTableModel::rowCount(const QModelIndex & /* parent */) const
{
    return AirportProvider::instance().getView().size();
}

int AirportTableModel::columnCount(const QModelIndex &/* parent */) const
{
    return 3;
}

QVariant AirportTableModel::data(const QModelIndex &index, int role) const
{
    const auto& apView = AirportProvider::instance().getView();
    int row = index.row();
    int col = index.column();
    const auto& selectedAp = apView[row];
    switch (role)
    {
    case Qt::DisplayRole:
    {
        if (col == 0)
            return selectedAp->mName;
        else if (col == 1)
            return selectedAp->mIcaoCode;
        else if (col == 2)
            return selectedAp->mCountry;
        else
            return QVariant{};
    }
    case Qt::UserRole:
        return QVariant::fromValue(apView[row]);
    }
    return QVariant{};
}

QVariant AirportTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section) {
        case 0:
            return QString("Название");
        case 1:
            return QString("Код ИКАО");
        case 2:
            return QString("Страна");
        }
    }
    return QVariant{};
}

Qt::ItemFlags AirportTableModel::flags(const QModelIndex &index) const
{
    return QAbstractTableModel::flags(index);
}
