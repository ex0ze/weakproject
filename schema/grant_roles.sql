grant select on all tables in schema public to airport_user;
grant select, insert, update on users, flights, passengers to airport_user;
grant usage, select on all sequences in schema public to airport_user;
grant select, insert, update, delete on all tables in schema public to airport_admin;
grant usage, select on all sequences in schema public to airport_admin;