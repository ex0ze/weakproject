drop owned by airport_user cascade;
drop owned by airport_admin cascade;
drop owned by postgres cascade;
drop database if exists airport_db;