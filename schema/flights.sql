create table if not exists flights
(
    id serial primary key,
    departure_time timestamp,
    departure_airport_id integer references airports(id),
    arrival_time timestamp,
    arrival_airport_id integer references airports(id) check ( departure_airport_id <> flights.arrival_airport_id ),
    flight_number varchar(30) not null unique,
    airline_id integer references airlines(id),
    plane_id integer references planes(id)
);

create or replace function create_flight(
departure_time_ timestamp without time zone,
departure_airport_id_ integer,
arrival_time_ timestamp without time zone,
arrival_airport_id_ integer,
flight_number_ character varying,
airline_id_ integer,
plane_id_ integer) returns boolean as
$$
declare
    cnt integer;
    flight_id integer;
    begin
        with _rows_ as (
         insert into flights(departure_time, departure_airport_id, arrival_time, arrival_airport_id, flight_number, airline_id, plane_id) values
            (departure_time_, departure_airport_id_, arrival_time_, arrival_airport_id_, flight_number_, airline_id_, plane_id_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            select currval(pg_get_serial_sequence('flights','id')) into flight_id;
            insert into plane_seats_price(flight_id, plane_seat_id, price)
            select flight_id, ps.id, 0 from plane_seats ps where ps.id_plane = plane_id_;
            return true;
        end if;
        return false;
    end;
$$ language plpgsql;
