-- уровень разрешений у пользователя
create table if not exists permission_level
(
    id serial primary key,
    -- название уровня разрешений - 'пользователь' / 'администратор'
    name varchar(50) not null,
     -- численная величина уровня разрешений
    p_level integer not null
);

insert into permission_level(name, p_level) values('Пользователь', 1);
insert into permission_level(name, p_level) values('Администратор', 2);