create table if not exists plane_seats_price
(
    id serial primary key,
    flight_id integer references flights(id),
    plane_seat_id integer references plane_seats(id),
    price money
);

create or replace function install_plane_seats_price(flight_id_ integer, plane_seat_id_ integer, price_ money)
returns bool as $$
    declare cnt integer;
    begin
        with _rows_ as (
         insert into plane_seats_price(flight_id, plane_seat_id, price) values
            (flight_id_, plane_seat_id_, price_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            return true;
        end if;
        return false;
    end;
$$ language plpgsql;

create or replace view detailed_plane_seats_prices  as
select psp.id, psp.flight_id,
    st.name as "Тип",
    ps.seats_count as "Количество мест",
    psp.price as "Цена" from plane_seats_price psp
    join plane_seats ps on psp.plane_seat_id = ps.id
    join seat_types st on ps.id_seat_type = st.id;

create or replace view detailed_plane_seats_prices2 as
select psp.flight_id, ps.id, st.name,
       ( select count (*) from passengers p where p.plane_seat_id = ps.id and p.flight_id = psp.flight_id) as booked_seats_count,
       ps.seats_count as all_seats_count,
       psp.price from plane_seats_price psp
join plane_seats ps on psp.plane_seat_id = ps.id
join seat_types st on ps.id_seat_type = st.id;



create or replace rule detailed_plane_seats_prices_update as on update to detailed_plane_seats_prices do instead (
    update plane_seats_price
    set price = new."Цена"
    where id = new.id;
    );