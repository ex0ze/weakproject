create table if not exists airlines
(
    id serial primary key,
    name varchar(200) not null unique,
    legal_address varchar(500)
);

create or replace function create_airline(name_ varchar, legal_address_ varchar)
returns bool as $$
    declare cnt integer;
    begin
        with _rows_ as (
         insert into airlines(name, legal_address) values
            (name_, legal_address_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            return true;
        end if;
        return false;
    end;
$$ language plpgsql;