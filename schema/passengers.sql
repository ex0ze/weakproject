-- таблица пассажиров
create table if not exists passengers
(
    id serial primary key,
    -- id пользователя
    user_id integer references users(id),
    -- id рейса
    flight_id integer references flights(id),
    -- id места в самолёте
    plane_seat_id integer references plane_seats(id),
    -- количество мест багажа
    pieces_of_baggage integer not null
);

create or replace function book_ticket(user_id_ integer, flight_id_ integer, plane_seat_id_ integer, pieces_of_baggage_ integer)
returns bool as $$
    declare cnt integer;
    begin
        with _rows_ as (
         insert into passengers(user_id, flight_id, plane_seat_id, pieces_of_baggage) values
            (user_id_, flight_id_, plane_seat_id_, pieces_of_baggage_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            return true;
        end if;
        return false;
    end;
$$ language plpgsql;
