-- пользователь системы
create table if not exists users
(
    id serial primary key,
    -- имя пользователя
    username varchar(50) unique not null,
    --пароль в Keccak256
    password varchar(64) not null,
    -- фио
    full_name text not null,
    -- дата рождения
    date_of_birth date not null,
    -- разрешения
    permission_level_id integer references permission_level(id)
);

-- возвращаемый тип функции
create type ap_login_ret as (plevel integer, namelevel varchar(50), fullname text, dateofbirth date);
-- функция попытки логина в систему аэропорта
-- в случае успеха возвращает (уровень привилегий, название уровня привелегий, имя пользователя)
create or replace function ap_login(login varchar, pass varchar)
returns ap_login_ret as $$
    declare ret ap_login_ret;
begin
        select pl.p_level, pl.name, u.full_name, u.date_of_birth into ret.plevel, ret.namelevel, ret.fullname, ret.dateofbirth from users as u
        left join permission_level pl on u.permission_level_id = pl.id
        where u.username = login and u.password = pass;
        if ret.fullname is null then
            return null;
            else
            return ret;
        end if;
end;
$$ language plpgsql;

--функция регистрации нового пользователя
--возвращает результат регистрации
create or replace function ap_register
    (login varchar, pass varchar, fullname text, birthdate date, permission_level integer)
    returns bool as $$
    declare ucount integer;
    declare plevelid integer;
    begin
        select p.id into plevelid from permission_level as p where p.p_level = permission_level;
        if plevelid is null then
            return false;
        end if;
        select count(*) into ucount from users where users.username = login;
        if ucount > 0 then
            return false;
        end if;
         insert into users(username, password, full_name, date_of_birth, permission_level_id)
            values (login, pass, fullname, birthdate, plevelid);
        return true;
    end;
$$ language plpgsql;

create function change_password(id_user integer, old_pass varchar, new_pass varchar) returns boolean
    language plpgsql
as
$$
declare ret_password varchar;
    begin
        update users set password = new_pass where id = id_user and password = old_pass returning password into ret_password;
        if new_pass = ret_password then
            return true;
        end if;
        return false;
    end;
$$;


--hash = admin_super_password
select ap_register('admin', 'f145906c4bc7534ea44ceb7510114059b0a8e3456c4dd7be4221b9816cc5d7df', 'Administrator', '01.01.1970', 2);