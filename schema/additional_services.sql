create table if not exists additional_services
(
    id serial primary key,
    flight_id integer references flights(id),
    passenger_id integer references passengers(id),
    description text
);


create or replace function insert_additional_service(flight_id_ integer, passenger_id_ integer, description_ text)
returns bool as $$
    declare cnt integer;
    begin
        with _rows_ as (
         insert into additional_services(flight_id, passenger_id, description) values
            (flight_id_, passenger_id_, description_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            return true;
        end if;
        return false;
    end;
$$ language plpgsql;