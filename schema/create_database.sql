create database airport_db
with owner = postgres
encoding = 'UTF-8'
tablespace = pg_default
connection limit = -1;
\c airport_db;