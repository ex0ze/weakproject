create or replace view flights_users_prices_summary as
select p.user_id, f.id as flight_id, a1.id as "id аэропорта вылета",
       a1.name as "Аэропорт вылета", a2.id as "id аэропорта прибытия",
       a2.name as "Аэропорт прибытия",
        st.name as "Тип билетов",
       psp.price as "Стоимость билета",
       count(p) as "Количество купленных билетов",
       sum(psp.price) as "Итого"
from users u
join passengers p on u.id = p.user_id
join flights f on f.id = p.flight_id
join airlines al on f.airline_id = al.id
join plane_seats on p.plane_seat_id = plane_seats.id
join seat_types st on plane_seats.id_seat_type = st.id
join plane_seats_price psp on f.id = psp.flight_id and plane_seats.id = psp.plane_seat_id
join airports a1 on f.departure_airport_id = a1.id
join airports a2 on f.arrival_airport_id = a2.id
group by p.user_id, st.id, f.id, a1.id, a2.id, psp.price
order by f.id;

create or replace view flights_seats_birth_year_stats as
    with temp as (
select extract(year from u.date_of_birth) as birth_year, f.id as flight_id, airp.country as country,
        st.name as seat_type_name,
       count(p) as tickets_count
from users u
join passengers p on u.id = p.user_id
join flights f on f.id = p.flight_id
join airlines al on f.airline_id = al.id
join plane_seats on p.plane_seat_id = plane_seats.id
join seat_types st on plane_seats.id_seat_type = st.id
join plane_seats_price psp on f.id = psp.flight_id and plane_seats.id = psp.plane_seat_id
join airports airp on f.departure_airport_id = airp.id
group by p.user_id, st.id, f.id, airp.id, psp.price, extract(year from u.date_of_birth))
select birth_year, seat_type_name, sum(tickets_count) as tickets_summary, country from temp group by tickets_count, birth_year, seat_type_name, country order by birth_year;


create or replace view detailed_flights as
    select flights.id,
    flights.flight_number as "Номер рейса",
    dep_a.name as "Аэропорт вылета",
    departure_time as "Время вылета",
    dep_b.name as "Аэропорт прилёта",
    arrival_time as "Время прилёта",
    al.name as "Авиакомпания",
    pl.name as "Самолёт",
    pl.tail_number as "Бортовой номер"
    from flights
    join airports dep_a on flights.departure_airport_id = dep_a.id
    join airports dep_b on flights.arrival_airport_id = dep_b.id
    join airlines al on flights.airline_id = al.id
    join planes pl on flights.plane_id = pl.id;


create or replace view get_departure_country_with_birthyear as
select distinct extract(year from u.date_of_birth) as birth_year, a.country from passengers p
join flights f on p.flight_id = f.id
join users u on p.user_id = u.id
join airports a on f.departure_airport_id = a.id;

create or replace view departure_airport_countries as
select distinct a.country from passengers p
join flights f on p.flight_id = f.id
join airports a on f.departure_airport_id = a.id;
select * from departure_airport_countries;
