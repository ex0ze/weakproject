create table if not exists airports
(
    id serial primary key,
    name varchar(300),
    icao_code varchar(10),
    country varchar(100),
    latitude float not null,
    longitude float not null
);

create or replace view decorated_airports as
select ap.id,
       ap.name as "Название",
       ap.icao_code as "Код ИКАО",
       ap.country as "Страна",
       latitude as "Широта",
       longitude as "Долгота"
from airports ap;