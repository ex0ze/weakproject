-- тип самолёта - например passenger / cargo(грузовой)
create table if not exists plane_types
(
    id serial primary key,
    name varchar(100) unique not null
);

insert into plane_types(name) values('Пассажирский');
insert into plane_types(name) values('Грузовой');
-- самолёт
create table if not exists planes
(
    id serial primary key,
    -- название самолёта
    name varchar(100),
    -- бортовой номер самолёта
    tail_number varchar(10) unique not null,
    -- тип самолёта
    id_type integer references plane_types(id)
);

create or replace function create_plane(name_ varchar, tail_number_ varchar, id_type_ integer)
returns bool as $$
    declare cnt integer;
    begin
        with _rows_ as (
         insert into planes(name, tail_number, id_type) values
            (name_, tail_number_, id_type_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            return true;
        end if;
        return false;
    end;
$$ language plpgsql;

create or replace view detailed_planes as
select p.id, p.name as "Название", p.tail_number as "Бортовой номер", pt.name as "Тип"
from planes p
join plane_types pt on pt.id = p.id_type;

-- тип места в самолёте regular / business / staff
create table if not exists seat_types
(
    id serial primary key,
    name varchar(20) unique not null
);

insert into seat_types(name) values('Эконом');
insert into seat_types(name) values('Комфорт');
insert into seat_types(name) values('Бизнес');

-- количество всяких разных мест в самолёте
-- например в самолёте с айди 1
-- 100 мест пассажирских
-- 30 мест бизнес
-- 10 мест для персонала / пилотов
create table if not exists plane_seats
(
    id serial primary key,
    -- айди самолёта
    id_plane integer references planes(id),
    -- тип места
    id_seat_type integer references seat_types(id),
    -- количество мест
    seats_count integer not null
);

create or replace function add_plane_seats(id_plane_ integer, id_seat_type_ integer, seats_count_ integer)
returns bool as $$
    declare cnt integer;
    declare current_count integer;
    begin
        select ps.seats_count into current_count from plane_seats ps where ps.id_plane = id_plane_ and ps.id_seat_type = id_seat_type_;
        if current_count is not null then
            update plane_seats set seats_count = current_count + seats_count_ where id_plane = id_plane_ and id_seat_type = id_seat_type_;
            return true;
            else
            with _rows_ as (
         insert into plane_seats(id_plane, id_seat_type, seats_count) values
            (id_plane_, id_seat_type_, seats_count_)
            returning 1
        )
        select count(*) into cnt from _rows_;
        if cnt > 0 then
            return true;
        end if;
        return false;
        end if;
    end;
$$ language plpgsql;

create or replace view detailed_plane_seats as
select p.id,
       st.name as "Тип места",
ps.seats_count as "Количество мест"
from plane_seats ps
join seat_types st on ps.id_seat_type = st.id
join planes p on ps.id_plane = p.id;
